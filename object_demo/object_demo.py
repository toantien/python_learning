class Employee:
   'Common base class for all employees'
   empCount = 0

   def __init__(self, name, salary):
      self.name = name
      self.salary = salary
      Employee.empCount += 1
   
   def displayCount(self):
     print "Total Employee %d" % Employee.empCount

   def displayEmployee(self):
      print "Name : ", self.name,  ", Salary: ", self.salary


emp1 = Employee("abc", 1)
emp2 = Employee("xyz", 2)

emp1.displayEmployee()
emp2.displayEmployee()

def main():
   print "Employee.__doc__:", Employee.__doc__
   print "Employee.__name__:", Employee.__name__
   print "Employee.__module__:", Employee.__module__
   print "Employee.__bases__:", Employee.__bases__
   print "Employee.__dict__:", Employee.__dict__
   print "Num of Employee: ", Employee.empCount

   print "Has Age:", hasattr(emp1,'age')

   emp1.age = 1

   print "Age: ", emp1.age

   print "Has Age:", hasattr(emp1,'age')

   if hasattr(emp1,'age') :
      print "Age: ", emp1.age
   else :
      print "None"

   setattr(emp1,'age', 8)

   print "Age", getattr(emp1,'age')

main()