#Basic about python
def printHelloWorld():
	print "Hello Python!"
	
def sum(a,b):
	return a + b
	
def myMax(a,b):
	if a >= b:
		return a
	return b
	
def demoForLoop():
	for x in range(0, 3):
		print "We're on time %d" % (x)
		
def demoWhileLoop():
	x = 0
	while x <= 3 :
		print "x = %d" % (x)
		x = x + 1
		
def demoList():
	listItem = {"abc", 1, "def"}
	for x in listItem :
		print x
		
print "Max of 2, 10, 100 is ", max(2,10,100)
	
printHelloWorld();

print "1 + 2 = ", sum(1,2);

print "Max of 2, 4 is ", myMax(2,4)

print "Max of 6, 4 is ", myMax(6,4)

demoForLoop()

demoWhileLoop()

demoList()