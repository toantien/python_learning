
class ClassA(object):
	'docstring for ClassA'
	arg = ""
	def __init__(self, arg):
		super(ClassA, self).__init__()
		self.arg = arg
	def printObject(self):
		print "Arg: ", self.arg
	def printObject2(self):
		print "Arg: ", self.arg,
	def __add__(self,other):
		return ClassA(self.arg + other.arg)
		
class ClassB(ClassA):
	'docstring for ClassB'
	def __init__(self, arg, arg1):
		super(ClassA, self).__init__()
		self.arg1 = arg1
		self.arg = arg
	def printObject2(self):
		print "Arg: ", self.arg, "; Arg1: ", self.arg1
	def __add__(self,other):
		return ClassB(self.arg + other.arg, self.arg1 + other.arg1)


def main():
	print "ClassA.__doc__:", ClassA.__doc__
	print "ClassA.__name__:", ClassA.__name__
	print "ClassA.__module__:", ClassA.__module__
	print "ClassA.__bases__:", ClassA.__bases__
	print "ClassA.__dict__:", ClassA.__dict__

	print "ClassB.__doc__:", ClassB.__doc__
	print "ClassB.__name__:", ClassB.__name__
	print "ClassB.__module__:", ClassB.__module__
	print "ClassB.__bases__:", ClassB.__bases__
	print "ClassB.__dict__:", ClassB.__dict__

	b = ClassB("test",2)
	b1 = ClassB("abc", 3)

	b.printObject()
	b.printObject2()

	b1.printObject2()
	c = b + b1

	c.printObject2()

main()